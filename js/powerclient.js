function PowerClient()
{
	this.url = "";
	this.socket = null;
}

PowerClient.prototype.connectTo = function( url, port )
{
	var that = this;
	this.url = url;
	if(!url)
		throw("You must specify the server URL of the PowerServer");

	if(this.socket)
		this.socket.close();

	if(typeof(WebSocket) === "undefined")
		WebSocket = window.MozWebSocket;
	if(typeof(WebSocket) === "undefined")
	{
		alert("Websockets not supported by your browser, consider switching to the latest version of Firefox, Chrome or Safari.");
		return;
	}

	//connect
	this.socket = new WebSocket("ws://"+url+":"+port+"/");
	this.socket.binaryType = "arraybuffer";
	this.socket.onopen = function(){  
		console.log("Socket has been opened! :)");  
	}

	this.socket.addEventListener("close", function(e) {
        localStorage.clear();
		console.log("Socket has been closed: ", e); 
		that.socket = null;
	});

	this.socket.onmessage = function(msg){

		let objectReceived = JSON.parse(msg.data);

        if (objectReceived.type === "userExistenceResponse") {

        	if (objectReceived.exist === true) {
                client.signInUser(getItemJson('googleUser'));
			} else {
        		// Fa el signout automàtic i mostra l'error per consola
                var auth2 = gapi.auth2.getAuthInstance();
                auth2.signOut().then(function () {
                    cleanLocalStorage();
                });

                document.querySelector('#any-attempt').classList.add('d-none');
				document.querySelector('#need-register-error').classList.remove('d-none');

        		console.log("Error. L'usuari no està registrat.")
			}
        }

		if (objectReceived.type === 'googleAuth') {

			if (objectReceived.authenticated) {

				if (objectReceived.userType === "public") {

                    userLoggedIn = new PublicLoggedIn();

                    userLoggedIn.type = objectReceived.type;
                    userLoggedIn.userType = objectReceived.userType;
                    userLoggedIn.id = objectReceived.id;
                    userLoggedIn.name = objectReceived.name;
                    userLoggedIn.surname = objectReceived.surname;
                    userLoggedIn.email = objectReceived.email;
                    userLoggedIn.imageUrl = objectReceived.imageUrl;
                    userLoggedIn.authenticated = objectReceived.authenticated;
                    userLoggedIn.lat = typeof objectReceived.lat !== 'undefined' ? objectReceived.lat : '';
                    userLoggedIn.long = typeof objectReceived.long !== 'undefined' ? objectReceived.long : '';
                    userLoggedIn.zipCode = typeof objectReceived.zipCode !== 'undefined' ? objectReceived.zipCode : '';
                    userLoggedIn.geolocationAllowed = typeof objectReceived.geolocationAllowed !== 'undefined' ? objectReceived.geolocationAllowed : false;

                    setItemJson("userLoggedIn", userLoggedIn);

                    if (getItemStr('actionUser') === 'signUp') {
                    	window.location.href = "../";
                    }

                    if (getItemStr('actionUser') === 'signIn') {
                    	window.location.href = "public";
                    }

                    removeItem('actionUser');

				} else if (objectReceived.userType === "admin") {

                    userLoggedIn = new AdminLoggedIn();

                    userLoggedIn.type = objectReceived.type;
                    userLoggedIn.userType = objectReceived.userType;
                    userLoggedIn.id = objectReceived.id;
                    userLoggedIn.name = objectReceived.name;
                    userLoggedIn.surname = objectReceived.surname;
                    userLoggedIn.email = objectReceived.email;
                    userLoggedIn.imageUrl = objectReceived.imageUrl;
                    userLoggedIn.storeId = typeof objectReceived.storeId !== 'undefined' ? objectReceived.storeId : null;
                    userLoggedIn.storeName = typeof objectReceived.storeName !== 'undefined' ? objectReceived.storeName : null;
                    userLoggedIn.authenticated = objectReceived.authenticated;

                    setItemJson("userLoggedIn", userLoggedIn);

                    if (getItemStr('actionUser') === 'signUp') {
                        window.location.href = "../";
                    }

                    if (getItemStr('actionUser') === 'signIn') {
                        window.location.href = "admin";
                    }

                    removeItem('actionUser');

				} else {
					console.log("Error al determinar el tipus d'usuari.")
				}

			} else {
				console.log("Error en l'autenticació de l'usuari.");
			}
		}

		if (objectReceived.type === 'storeSaved') {
            userLoggedIn.storeId = objectReceived.storeId;
            userLoggedIn.storeName = objectReceived.name;
            setItemJson("userLoggedIn", userLoggedIn);
            location.reload();
		}

		if (objectReceived.type === 'product' && objectReceived.action === 'setAllRelatedWith') {

            for (let i in objectReceived.products) {
                document.querySelector('#product-list').innerHTML = '';
                let productCard = document.querySelector(".product-card");
                let productCardCloned = productCard.cloneNode(true);

                let codiImatge = (objectReceived.products[i].image === null || objectReceived.products[0].image === '') ? '' : '<img class=\"card-img-top\" src=\"'+objectReceived.products[i].image+'\" alt=\"Foto de '+objectReceived.products[i].name+'\" title=\"'+objectReceived.products[i].name+'\">';
                let store = objectReceived.products[i].storeInfo;
                let textStock = objectReceived.products[i].stock > 0 ? 'En stock' : 'Fora de stock';
                let classStock = objectReceived.products[i].stock > 0 ? 'text-success' : 'text-danger';
                let botoReserva = objectReceived.products[i].stock > 0 ? '    <button type=\"button\" id=\"'+i+'\" class=\"card-link btn btn-warning float-right\">Reserva</button>' : '';

                getJSON('https://maps.googleapis.com/maps/api/distancematrix/json?origins='+userLoggedIn.lat+','+userLoggedIn.long+'&destinations='+store.latitude+','+store.longitude+'&language=ca&key=AIzaSyBLiWfawVu3_4Q8Iy8YoQlCFmVklQxM8Y4', function(err, data) {

                    productCardCloned.innerHTML = codiImatge +
                        '<div class=\"card-body\">' +
                        '    <h4 class=\"card-title\">'+objectReceived.products[i].name+'</h4>' +
                        '    <p class=\"card-text\"><i class=\"fas fa-map-marker-alt\" title=\"Ubicació de la botiga\"></i> '+store.address+', '+store.zipCode+', '+store.city+', '+store.country+'</p>' +
                        '    <p class=\"card-text\"><i class=\"fas fa-map-signs\" title=\"Distància de la botiga\"></i> A '+data.rows[0].elements[0].duration.text+' ('+data.rows[0].elements[0].distance.text+')'+'</p>' +
                        '    <p class=\"card-text\">'+objectReceived.products[i].description+'</p>' +
                        '    <p class=\"card-text float-left display-custom negreta\">'+objectReceived.products[i].price+'</p>' +
                        '    <p class=\"card-text text-right '+classStock+'\">'+textStock+'</p>' +
                        botoReserva +
                        '  </div>';

                    document.querySelector('#product-list').appendChild(productCardCloned);
                });

            }

            if (objectReceived.products.length === 0) {
                document.querySelector('#product-list').innerHTML = '';
                document.querySelector('#product-list').innerHTML = '<p class=\"text-danger\">No hem trobat cap resultat per la teva cerca</p>';
            }
        }

        if (objectReceived.type === 'product' && objectReceived.action === 'setAllProductsAdmin') {

            for (let i in objectReceived.products) {
                document.querySelector('#product-list').innerHTML = '';
                let productCard = document.querySelector(".product-card");
                let productCardCloned = productCard.cloneNode(true);

                let codiImatge = (objectReceived.products[i].image === null || objectReceived.products[0].image === '') ? '' : '<img class=\"card-img-top\" src=\"'+objectReceived.products[i].image+'\" alt=\"Foto de '+objectReceived.products[i].name+'\" title=\"'+objectReceived.products[i].name+'\">';

                let textStock = 'Unitats disponibles: ' + objectReceived.products[i].stock;

                productCardCloned.innerHTML = codiImatge +
                    '<div class=\"card-body\">' +
                    '    <h4 class=\"card-title\">'+objectReceived.products[i].name+'</h4>' +
                    '    <p class=\"card-text\">'+objectReceived.products[i].description+'</p>' +
                    '    <p class=\"card-text\">'+objectReceived.products[i].price+'</p>' +
                    '    <p class=\"card-text\">'+textStock+'</p>' +
                    '  </div>';

                document.querySelector('#product-list').appendChild(productCardCloned);


            }

        }
	}


	this.socket.onerror = function(err){  
		console.log("error: ", err );
	}

	return true;
}

PowerClient.prototype.sendSth = function( msg, target_ids )
{
	if(msg === null)
		return;

	if(msg.constructor === Object || msg instanceof PublicLoggedIn || msg instanceof AdminLoggedIn)
		msg = JSON.stringify(msg);

	if(!this.socket || this.socket.readyState !== WebSocket.OPEN)
	{
		console.error("Not connected, cannot send info");
		return;
	}

	//pack target info
	if( target_ids )
	{
		var target_str = "@" + (target_ids.constructor === Array ? target_ids.join(",") : target_ids) + "|";
		if(msg.constructor === String)
			msg = target_str + msg;
		else
			throw("targeted not supported in binary messages");
	}

	this.socket.send(msg);
	this.info_transmitted += 1;
}

PowerClient.prototype.addProduct = function( name, description, price, stock, image, amazon_key, store_id )
{
    var product = {
        type : "product",
        action : "add", // add, mod, del (afegir, modificar, eliminar)
        productId : null,
        name : name,
        description : description,
        price : price,
        stock : stock,
        image : image,
        amazonKey : amazon_key,
        storeId : store_id
    };

    client.sendSth(product);
}

PowerClient.prototype.getAllProductsRelatedWith = function( amazonTag )
{
    let product = {
        type : "product",
        action : "getAllRelatedWith",
        amazonTag : amazonTag
    };

    client.sendSth(product);
}

PowerClient.prototype.getAllProductsAdmin = function( storeId )
{
    let product = {
        type : "product",
        action : "getAllAdmin",
        storeId : storeId
    };

    client.sendSth(product);
}

PowerClient.prototype.modifyProduct = function( product_id, name, description, price, stock, amazon_key )
{
	
}

PowerClient.prototype.deleteProduct = function( product_id )
{
	
}

PowerClient.prototype.addStore = function( name, address, lat, long, zip_code, city, country, user_id )
{
    let store = {
        type : "store",
        action : "add", // add, mod, del (afegir, modificar, eliminar)
        storeId : null,
        name : name,
        address : address,
        latitude : lat,
        longitude : long,
        zipCode : zip_code,
        city : city,
		country : country,
		userId : user_id
    };

    client.sendSth(store);
}

PowerClient.prototype.authenticateUser = function ( google_user )
{
    let profile = google_user.getBasicProfile();

    let googleUser = {
        type : null,
        userType : null,
        tokenId : google_user.getAuthResponse().id_token,
        id : profile.getId(),
        name : profile.getName(),
        surname : profile.getGivenName(),
        email : profile.getEmail(),
        imageUrl : profile.getImageUrl(),
        authenticated : false
    };

	if (window.location.href.includes('register')) {

		client.signUpUser(googleUser);

	} else {

        let googleUserVerification = {
            type : "userExistence",
            id : profile.getId()
        };

        // Desem temporalment l'object googleUser amb les dades fins que comprovem si l'usuari ja té un compte creat
        setItemJson('googleUser', googleUser);

        // Aquí fem la crida per comprovar si l'usuari està donat d'alta
        client.sendSth(googleUserVerification);
	}

};

PowerClient.prototype.signInUser = function ( googleUser )
{
    googleUser.type = 'signInGoogle';

    setItemStr('actionUser','signIn');

    client.sendSth(googleUser);
};

PowerClient.prototype.signUpUser = function ( googleUser )
{
    googleUser.type = 'signUpGoogle';
    googleUser.userType = window.location.href.includes('admin') ? 'admin' : 'public';

    setItemStr('actionUser','signUp');

    client.sendSth(googleUser);
};

PowerClient.prototype.signOutUser = function ( google_user )
{
	//TODO: Cridar al servidor per avisar-lo del tancament de sessió
};

PowerClient.prototype.modifyUser = function( user )
{
    user.action = 'mod';

    client.sendSth(user);
}