// Si l'usuari no està loguejat, el redirigim a la plana inicial
if (userLoggedIn === null) {
    window.location.replace("../");
}

// Si l'usuari no és public, no li donem permís per accedir a la pàgina
if (userLoggedIn.userType !== 'public') {

    const notAuthorizedModal = document.querySelector("#modal-not-authorized");

    let notAuthorizedModalInstance = new Modal(notAuthorizedModal, {
        backdrop: "static",
        focus: true,
        keyboard: false
    });

    notAuthorizedModalInstance.show();

    const goBackBtn = document.querySelector('#go-back-btn');

    goBackBtn.addEventListener('click', function (e) {
        window.location.replace('../admin');
    });

    notAuthorizedModalInstance.show();
} else {

    document.querySelector('#user-name-surname').innerHTML = userLoggedIn.name;

    // Fem visible el dashboard de l'usuari public
    document.querySelector('#public-dashboard').classList.remove("d-none");

    const requestLocationButton = document.querySelector('#location-btn');

    // Si no accepta, en qualsevol moment podrà tornar a demanar el permís fent click al botó d'ubicació o bé si la vol tornar a demanar
    requestLocationButton.addEventListener('click', function (e) {
        e.preventDefault();
        showModalGeolocationPermission();
    });

    if (userLoggedIn.geolocationAllowed) {
        getAndUpdateGeolocationUser();

        let searchField = document.querySelector('#search-field');

        searchField.addEventListener("keyup", function(e) {
            //e.preventDefault();

            if (e.key === "Enter") {
                let linkIntroduced = analizeAmazonUrl(searchField.value);
                client.getAllProductsRelatedWith(linkIntroduced);
            }
        });

    } else {

        let searchField = document.querySelector('#search-field');

        searchField.addEventListener("keyup", function(e) {
            //e.preventDefault();

            if (e.key === "Enter") {

                if (!userLoggedIn.geolocationAllowed) {
                    showModalGeolocationPermission();
                }
            }
        });
    }

    /*const testButton = document.querySelector('#reservations-btn');

    testButton.addEventListener('click', function (e) {
        e.preventDefault();

        //getDistance();
    });*/

}

function showModalGeolocationPermission() {

    const locationPermissionModal = document.querySelector('#modal-location-permission');

    if (userLoggedIn.geolocationAllowed) {
        document.querySelector('#geolocation-permission').classList.add('d-none');
        document.querySelector('#allow-geoloc-permission-btn').classList.add('d-none');
        document.querySelector('#geolocation-attempt').classList.remove('d-none');
        document.querySelector('#new-attempt-geoloc-permission-btn').classList.remove('d-none');
    }

    let locationPermissionModalInstance = new Modal(locationPermissionModal, {
        focus: true,
    });

    locationPermissionModalInstance.show();

    const allowLocationPermissionBtn = document.querySelector('#allow-geoloc-permission-btn');

    allowLocationPermissionBtn.addEventListener('click', function (e) {
        e.preventDefault();
        userLoggedIn.geolocationAllowed = true;
        getAndUpdateGeolocationUser();
        locationPermissionModalInstance.hide();

        let searchField = document.querySelector('#search-field');
        let linkIntroduced = analizeAmazonUrl(searchField.value);
        client.getAllProductsRelatedWith(linkIntroduced);
    });

    const newAttemptLocationPermissionBtn = document.querySelector('#new-attempt-geoloc-permission-btn');

    newAttemptLocationPermissionBtn.addEventListener('click', function (e) {
        e.preventDefault();
        userLoggedIn.geolocationAllowed = true;
        getAndUpdateGeolocationUser();
        locationPermissionModalInstance.hide();
    });
}

function getAndUpdateGeolocationUser()
{
    getJSON('http://ip-api.com/json?lang=es',
        function(err, data) {
            if (err === null) {

                setItemStr('lat', data.lat);
                setItemStr('long', data.lon);

                getJSON('https://maps.googleapis.com/maps/api/geocode/json?latlng='+data.lat+','+data.lon+'&key=AIzaSyBLiWfawVu3_4Q8Iy8YoQlCFmVklQxM8Y4', function(err, data) {

                    userLoggedIn.lat = data.results[0].geometry.location.lat;
                    userLoggedIn.long = data.results[0].geometry.location.lng;
                    userLoggedIn.zipCode = data.results[0].address_components[6].long_name;

                    client.modifyUser(userLoggedIn);
                });

            } else {
                alert('Ha hagut un problema en obtenir la teva ubicació: ' + err);
            }
        });
}
