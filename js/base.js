var client = new PowerClient();

client.connectTo("84.89.136.194", "9044");

let getItemJson = function( key ) {
    return JSON.parse(localStorage.getItem(key));
};

let setItemJson = function( key, obj ) {
    localStorage.setItem(key, JSON.stringify(obj));
};

let getItemStr = function( key ) {
    return localStorage.getItem(key);
};

let setItemStr = function( key, str ) {
    localStorage.setItem(key, str);
};

let removeItem = function( key ) {
    localStorage.removeItem(key);
};

let cleanLocalStorage = function () {
    localStorage.clear();
};

function PublicLoggedIn() {
    this.type = '';
    this.userType = '';
    this.id = '';
    this.name = '';
    this.surname = '';
    this.email = '';
    this.imageUrl = '';
    this.authenticated = false;
    this.lat = '';
    this.long = '';
    this.zipCode = '';
    this.geolocationAllowed = false;
}

function AdminLoggedIn() {
    this.type = '';
    this.userType = '';
    this.id = '';
    this.name = '';
    this.surname = '';
    this.email = '';
    this.imageUrl = '';
    this.authenticated = false;
    this.storeId = null;
    this.storeName = null;
}

let userLoggedIn = null;

// Recuperem les dades després d'haver iniciat la sessió
if (getItemJson("userLoggedIn") !== 'null') {
    userLoggedIn = getItemJson('userLoggedIn');
}

// inici de sessió via Google
function signIn(googleUser) {
    client.authenticateUser(googleUser);
}

// Només inicialitzem aquest codi en cas d'haver-se iniciat la sessió
if (userLoggedIn !== null) {

    const signOutButton = document.querySelector("button#sign-out-google");

    signOutButton.addEventListener("click", function(e) {
        signOut();
    });

}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        cleanLocalStorage();
        window.location.href = "../";
    });
}

// Mètode GET per obtenir JSONs
let getJSON = function(url, callback) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
        let status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};

// Donada una url completa o un tag d'un producte d'Amazon, tracta les dades per acabar obtenint només el tag
let analizeAmazonUrl = function(link) {
    let productAmazonTag = null;

    if (link.includes('http') || link.includes('https')) {

        let linkSplitted = link.split('/');

        let posicioTag = 0;

        for (let i in linkSplitted) {
            if (linkSplitted[i] === 'product' || linkSplitted[i] === 'dp') {
                posicioTag = parseInt(i)+1;
            }
        }

        productAmazonTag = linkSplitted[posicioTag];

    } else {
        productAmazonTag = link;
    }

    return productAmazonTag;
}