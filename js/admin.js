// Si l'usuari no està loguejat, el redirigim a la plana inicial
if (userLoggedIn === null) {
    window.location.replace("../");
}

// Si l'usuari no és admin, no li donem permís per accedir a la pàgina
if (userLoggedIn.userType !== 'admin') {

    const notAuthorizedModal = document.querySelector("#modal-not-authorized");

    let notAuthorizedModalInstance = new Modal(notAuthorizedModal, {
        backdrop: "static",
        focus: true,
        keyboard: false
    });

    notAuthorizedModalInstance.show();

    const goBackBtn = document.querySelector('#go-back-btn');

    goBackBtn.addEventListener('click', function (e) {
        window.location.replace('../public');
    });
}

// Si l'usuari té storeId és que ja està registrat
if (userLoggedIn.storeId !== null) {

    document.querySelector('#user-store-name').innerHTML = userLoggedIn.storeName;

    client.getAllProductsAdmin(userLoggedIn.storeId);

    // Fem visible el dashboard de l'usuari admin
    document.querySelector('#admin-dashboard').classList.remove("d-none");

    const addProductModal = document.querySelector("#modal-add-product");

    let addProductModalInstance = new Modal(addProductModal, {
        focus: true
    });

    let field = document.querySelector("#someField");

    const addProductBtn = document.querySelector('#btn-add-product');

    addProductBtn.addEventListener('click', function (e) {

        addProductModalInstance.show();

    });

    const saveProductBtn = document.querySelector('#save-product');

    saveProductBtn.addEventListener('click', function (e) {

        e.preventDefault();

        let name = document.querySelector('#product-name').value;
        let description = document.querySelector('#product-description').value;
        let price = document.querySelector('#product-price').value;
        let currency = document.querySelector('#product-currency').value;
        let stock = document.querySelector('#product-stock').value;
        let image = document.querySelector('#product-image').value;
        let amazonLink = document.querySelector('#amazon-link').value;

        currency = analizeCurrency(currency);

        let amazonTag = analizeAmazonUrl(amazonLink);

        client.addProduct(name, description, price+currency, stock, image, amazonTag, userLoggedIn.storeId);

        addProductModalInstance.hide();

        // Ho utilitzem per netejar els formularis
        document.querySelector('#add-product-form').reset();

        client.getAllProductsAdmin(userLoggedIn.storeId);
    });

} else { // Si l'usuari no té storeId és que és el seu prime inici de sessió

    // Modal per complimentar les dades de la botiga
    const endRegisterModal = document.querySelector("#modal-complete-fields");

    let endRegisterModalInstance = new Modal(endRegisterModal, {
        backdrop: "static",
        focus: true,
        keyboard: false
    });

    endRegisterModalInstance.show();
    /**********************************************************************/

    // Obtenció de la ubicació en cas que l'usuari ho sol·liciti
    const geolocationBtn = document.querySelector('#btn-geolocation');

    geolocationBtn.addEventListener('click', function (e) {

        getJSON('http://ip-api.com/json?lang=es',
            function(err, data) {
                if (err === null) {

                    setItemStr('lat', data.lat);
                    setItemStr('long', data.lon);

                    getJSON('https://maps.googleapis.com/maps/api/geocode/json?latlng='+data.lat+','+data.lon+'&key=AIzaSyBLiWfawVu3_4Q8Iy8YoQlCFmVklQxM8Y4', function(err, data) {
                        //TODO: COMPROVAR SI EL TYPE ÉS EL CORRECTE -> https://stackoverflow.com/questions/13551593/get-a-address-components?rq=1
                        document.querySelector('#address-store').value = data.results[0].address_components[1].long_name + ', ' + data.results[0].address_components[0].long_name;
                        document.querySelector('#zip-store').value = data.results[0].address_components[6].long_name;
                        document.querySelector('#city-store').value = data.results[0].address_components[2].long_name;
                        document.querySelector('#country-store').value = data.results[0].address_components[5].long_name;

                        document.querySelector('#address-store').onchange();
                        document.querySelector('#zip-store').onchange();
                        document.querySelector('#city-store').onchange();
                        document.querySelector('#country-store').onchange();
                    });

                } else {
                    alert('Ha hagut un problema en obtenir la teva ubicació: ' + err);
                }
            });

    });
    /**********************************************************************/

    // Es desen les dades de la botiga i s'envien al servidor
    const saveStoreInfoBtn = document.querySelector('#send-store-info');

    saveStoreInfoBtn.addEventListener('click', function (e) {

        e.preventDefault();

        let name = document.querySelector('#name-store').value;
        let address = document.querySelector('#address-store').value;
        let latitude = getItemStr('lat');
        let longitude = getItemStr('long');
        let zip = document.querySelector('#zip-store').value;
        let city = document.querySelector('#city-store').value;
        let country = document.querySelector('#country-store').value;

        removeItem('lat');
        removeItem('long');

        client.addStore(name, address, latitude, longitude, zip, city, country, userLoggedIn.id);

        endRegisterModalInstance.hide();
    });
    /**********************************************************************/

}

// Donada una ref de moneda, ho convertim al símbol de la moneda
function analizeCurrency(currency) {
    if (currency === 'euro') {
        currency = '€';
    } else if (currency === 'dolar') {
        currency = '$';
    } else if (currency === 'lliure') {
        currency = '£';
    } else {
        currency = 'u.m.';
    }
    return currency;
}