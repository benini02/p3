Albert Pina Calmet
NIA: 164376

Instrucciones:
1) Usar Chrome (con Firefox no funciona, entra en bucle y tira el socket)
2) Solo se permiten cuentas gmail para iniciar sesión y registrarse
3) Si te registras como vendedor y luego comprador (o viceversa), se machaca el registro del usuario anterior.
Por lo tanto, recomiendo usar una sesión privada y otra normal para poder tener dos cuentas distintas de gmail.
4) Para que la API de Google Matrix Distance, usar esta extensión (probada por mi) o una parecida: https://chrome.google.com/webstore/detail/cors/dboaklophljenpcjkbbibpkbpbobnbld
5) El servidor está iniciado, pero sino funcionara o quieres reiniciarlo, mata el proceso 13095 y
vuelvelo a iniciar yendo a /www/p3/server-side y ejecutar el siguiente comando: nodejs powerserver.js

Detalles a tener en cuenta:
- Botón "Reserva" de los productos no hace nada
- Botones "Compres" y "Reserves" no cargan nada
- Cuando buscas un producto, la primera vez falla. Hay que recargar la página
- El apartado "Els meus productos" de admin no siempre carga los productos del admin
- Tanto si eres comprador como si eres vendedor, en el espacio para introducir el producto de Amazon
lo puedes poner en cualquiera de sus formas, URL completa, solo el Amazon Tag, url con parametros, etc.

Productos introducidos por defecto al iniciar el servidor:
https://www.amazon.es/Samsung-Galaxy-Plus-Smartphone-Bluetooth/dp/B079XF3BYL/
https://www.amazon.es/Gonher-Pistola-Juguete-para-Ni%C3%B1os/dp/B000MLQFEW/

URL: http://2014-21305.esup.upf.edu/students/2018/benini/p3/