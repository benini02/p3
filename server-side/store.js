function StoreLib( redisClient ) {
	this.redisClient = redisClient;
	this.storeIdCounter = 0;
}

StoreLib.prototype.addStore = function( object, storeId ) 
{
    object.storeId = storeId;
    object.products = [];

	let objectStr = JSON.stringify(object);

	this.redisClient.hset('stores', storeId, objectStr);
	this.redisClient.hget('stores', storeId, function(err, obj) {
        console.log(JSON.parse(obj));
	});
}

StoreLib.prototype.addProductToStore = function( productId, storeId )
{
	let that = this;
    this.redisClient.hget('stores', storeId, function(err, obj) {
        let store = JSON.parse(obj);
        store.products.push(productId);
        that.redisClient.hset('stores', storeId, JSON.stringify(store));
    });
}

StoreLib.prototype.calculateStoreId = function( object, callback )
{	
	var mainInstance = this;

	this.redisClient.incr('storeIdCounter');

	this.redisClient.get('storeIdCounter', function(err, value) {
		mainInstance.updateStoreIdCounter(value);
	});

    callback(object, this.storeIdCounter);

    return this.storeIdCounter;
}

StoreLib.prototype.updateStoreIdCounter = function(val)
{
	this.storeIdCounter = val;
}

module.exports = StoreLib;