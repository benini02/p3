const WebSocket = require('ws');
const Redis = require('redis');
const ProductLib = require('./product');
const StoreLib = require('./store');
const UserLib = require('./user');

const wss = new WebSocket.Server( { port:9044 } );

PowerServer();

function PowerServer () {

	this.ws = null;

	this.redisClient = Redis.createClient(); //new redisClient

    this.redisClient.del('productIdCounter', function(err, reply) {
		console.log("deleting... productIdCounter");
	});

    this.redisClient.del('storeIdCounter', function(err, reply) {
        console.log("deleting... storeIdCounter");
    });

    this.redisClient.del('products', function(err, reply) {
        console.log("deleting... products");
    });

    this.redisClient.del('amazonTags', function(err, reply) {
        console.log("deleting... amazonTags");
    });

    this.redisClient.del('stores', function(err, reply) {
        console.log("deleting... stores");
    });

    this.redisClient.del('users', function(err, reply) {
        console.log("deleting... users");
    });

    this.redisClient.on('connect', function() {
	    console.log('connected on redis');
	});

    let user1 = { id: '105232105123841798758',
        type: 'googleAuth',
        userType: 'admin',
        name: 'Josep Pla',
        surname: 'Pla',
        email: 'j.o.s.e.pla@gmail.com',
        imageUrl: '',
        authenticated: false };

    this.redisClient.hset('users', '105232105123841798758', JSON.stringify(user1));

    let galaxyS9PlusAzul = {"type":"product","productId":1001,"name":"Samsung Galaxy S9 Plus","description":"-Cámara trasera Dual Pixel Super Slow-mo 12MP OIS (F1.5/F2.4) + 12MP OIS. Cámara frontal 8MP AF F1.7<br>" +
        "-6 GB de RAM, almacenamiento de 64 GB + ranura MicroSD (hasta 256 GB)<br>" +
        "-Funciones avanzadas: IP68 (resistente al agua y al polvo), Carga rápida, Samsung Pay y Sistema de huella dactilar, reconocimiento facial, escaner de iris. Bixby<br>" +
        "-Versión española: incluye Samsung Pay, acceso a promociones Samsung Members y recibe actualizaciones de software oficiales y homologadas ","price":"800€","stock":"5","image":"https://images-na.ssl-images-amazon.com/images/I/71a3T99bzjL._SL1500_.jpg","amazonKey":"B079XF3BYL","storeId":500};

    let pistolaJuguete = {"type":"product","productId":1002,"name":"Pistola Gonher 80/0","description":"Revolver de vaquero de metal de 8 disparos. Revolver fabricado en España. Desarrolla la imaginación en el juego y la adopción de roles.","price":"5€","stock":"0","image":"https://images-na.ssl-images-amazon.com/images/I/41HRj3u2%2BYL.jpg","amazonKey":"B000MLQFEW","storeId":500};

    this.redisClient.hset('products', '1001', JSON.stringify(galaxyS9PlusAzul));
    this.redisClient.hset('products', '1002', JSON.stringify(pistolaJuguete));

    let galaxyS9related = {
        products : []
    };
    galaxyS9related.products.push(1001);
    this.redisClient.hset('amazonTags', 'B079XF3BYL', JSON.stringify(galaxyS9related));

    let revolverRelated = {
        products : []
    };
    revolverRelated.products.push(1002);
    this.redisClient.hset('amazonTags', 'B000MLQFEW', JSON.stringify(revolverRelated));

    let store1 = { type: 'store',
        storeId: 500,
        name: 'Tech Shop',
        address: 'Avinguda Diagonal, 550',
        latitude: '41.3945502',
        longitude: '2.1485466',
        zipCode: '08021',
        city: 'Barcelona',
        country: 'España',
        userId: '105232105123841798758',
        products: ['1001','1002'] };

    this.redisClient.hset('stores', '500', JSON.stringify(store1));

    this.productLib = new ProductLib(this.redisClient);
    this.storeLib = new StoreLib(this.redisClient);
    this.userLib = new UserLib(this.redisClient);

   // let that = this;

    /*this.redisClient.get('productIdCounter', function(err, value) {
		if (!value) {
			console.log('creating new one productIdCounter...');
            that.redisClient.set('productIdCounter', 0);
		}
	});*/

	/*redisClient.hmset('clients', 'cl1', 'albert', 'cl2', 'Bootstrap');

	redisClient.hget('clients', 'cl1', function(err, object) {
	    console.log(object);
	});

	redisClient.hmset('clients', 'cl3', 'pepe', 'cl4', 'test');
	redisClient.hget('clients', 'cl1', function(err, object) {
	    console.log(object);
	});
	redisClient.hget('clients', 'cl3', function(err, object) {
	    console.log(object);
	});

	//redisClient.set('name', 'Albert');
	redisClient.get('name', function(err,v) {
	    console.log('name is ' + v);
	});*/
}

/*wss.on('request', function(request) {

    let connection = request.accept(null, request.origin);
    console.log("NEW WEBSOCKET USER!!!");
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log( "NEW MSG: " + message.utf8Data ); // process WebSocket message
        }
    });

    connection.on('close', function(connection) {
        console.log("USER IS GONE");// close user connection
    });
});*/

let that = this;

wss.on('connection', function connection(ws) {

    that.ws= ws;

    //console.log('user connected');

    that.ws.on('message', function incoming(message) {

        let objectReceived = JSON.parse(message);

        if (objectReceived.type === 'product') {
            processProduct(objectReceived);
        } else if (objectReceived.type === 'store') {
            processStore(objectReceived);
        } else if (objectReceived.type === 'userExistence') {
            processUserExistence(objectReceived);
        } else if (objectReceived.type === 'signUpGoogle') {
            processSignUpGoogle(objectReceived);
        } else if (objectReceived.type === 'signInGoogle') {
            processSignInGoogle(objectReceived);
        } else if (objectReceived.type === 'signOutGoogle') {
            processSignOutGoogle(objectReceived);
        } else if (objectReceived.type === 'googleAuth') {
            processUser(objectReceived);
        }
    });


});

// Es tramita les altes, modificacions i eliminacions de productes
function processProduct ( objectReceived )
{
    if (objectReceived.action === "add") {
        delete objectReceived.action;
        objectReceived.productId = this.productLib.calculateProductId(objectReceived, this.productLib.addProduct);
        this.storeLib.addProductToStore(objectReceived.productId, objectReceived.storeId);
    } else if (objectReceived.action === "mod") {
        delete objectReceived.action;

    } else if (objectReceived.action === "del") {

    } else if (objectReceived.action === 'getAllAdmin') {
        this.productLib.getAllProductsAdmin(objectReceived.storeId, that.ws);
    } else if (objectReceived.action === 'getAllRelatedWith') {
        this.productLib.getAllProductsRelatedWith(objectReceived.amazonTag, that.ws);
    }
}
/**********************************************************************/

// Es tramita les altes, modificacions i eliminacions de botigues
function processStore ( objectReceived )
{
    if (objectReceived.action === "add") {
        delete objectReceived.action;
        objectReceived.storeId = this.storeLib.calculateStoreId(objectReceived, this.storeLib.addStore);
        this.userLib.addStoreToUser(objectReceived.storeId, objectReceived.name, objectReceived.userId);
        objectReceived.type = "storeSaved";
        that.ws.send(JSON.stringify(objectReceived));
    } else if (objectReceived.action === "mod") {
        delete objectReceived.action;

    } else if (objectReceived.action === "del") {

    }
}/**********************************************************************/

// Es comprova si l'usuari existeix
function processUserExistence ( objectReceived )
{
    this.userLib.checkIfUserExist(objectReceived.id, that.ws);
}
/**********************************************************************/

// Es tramita l'alta de l'usuari a través del compte de Google
function processSignUpGoogle ( objectReceived )
{
    const thatInThisContext = this;

    const {OAuth2Client} = require('google-auth-library');
    const client = new OAuth2Client('941397949982-080lvcecgic6iqi39mjalte38gjnild9.apps.googleusercontent.com');
    async function verify() {
        const ticket = await client.verifyIdToken({
            idToken: objectReceived.tokenId,
            audience: '941397949982-080lvcecgic6iqi39mjalte38gjnild9.apps.googleusercontent.com',  // Specify the CLIENT_ID of the app mainThat accesses the backend
            // Or, if multiple clients access the backend:
            //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
        });
        const payload = ticket.getPayload();
        const userid = payload['sub'];
        // If request specified a G Suite domain:
        //const domain = payload['hd'];
        let user = {
            id : objectReceived.id,
            type : 'googleAuth',
            userType : objectReceived.userType,
            name : objectReceived.name,
            surname : objectReceived.surname,
            email : objectReceived.email,
            imageUrl : objectReceived.imageUrl,
            authenticated : true
        };

        that.ws.send(JSON.stringify(user));

        thatInThisContext.userLib.addUser(user, userid);

    }

    verify().catch(console.error);
}
/**********************************************************************/

// Es valida i s'obtenen les dades de l'usuari per iniciar sessió a partir de Google
function processSignInGoogle ( objectReceived )
{
    const thatInThisContext = this;

    const {OAuth2Client} = require('google-auth-library');
    const client = new OAuth2Client('941397949982-080lvcecgic6iqi39mjalte38gjnild9.apps.googleusercontent.com');
    async function verify() {
        const ticket = await client.verifyIdToken({
            idToken: objectReceived.tokenId,
            audience: '941397949982-080lvcecgic6iqi39mjalte38gjnild9.apps.googleusercontent.com',  // Specify the CLIENT_ID of the app mainThat accesses the backend
            // Or, if multiple clients access the backend:
            //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
        });
        const payload = ticket.getPayload();
        const userid = payload['sub'];
        // If request specified a G Suite domain:
        //const domain = payload['hd'];

        thatInThisContext.userLib.getUser(userid, that.ws);
    }

    verify().catch(console.error);
}
/**********************************************************************/

// Es tanca la sessió de Google
function processSignOutGoogle( objectReceived )
{
    // TODO: implementar lògica de tancament de sessió pq el servidor se n'enteri
}
/**********************************************************************/

// Processa les peticions que tenen a veure amb els usuaris
function processUser( objectReceived )
{
    if (objectReceived.action === 'mod') {
        this.userLib.modifyUser(objectReceived, that.ws);
    }
}

/**********************************************************************/

module.exports.PowerServer = PowerServer;