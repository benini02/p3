function UserLib( redisClient ) {
	this.redisClient = redisClient;
}

UserLib.prototype.addUser = function( object, userId )
{

	let objectStr = JSON.stringify(object);

	this.redisClient.hset('users', userId, objectStr);
	this.redisClient.hget('users', userId, function(err, obj) {
        console.log(JSON.parse(obj));
	});
}

UserLib.prototype.addStoreToUser = function( storeId, storeName, userId )
{
    let that = this;
    this.redisClient.hget('users', userId, function(err, obj) {
        let user = JSON.parse(obj);
        user.storeId = storeId;
        user.storeName = storeName;
        that.redisClient.hset('users', userId, JSON.stringify(user));
    });
}

UserLib.prototype.checkIfUserExist = function (userId, ws)
{
    this.redisClient.hget('users', userId, function(err, obj) {
        let tmpObj = null;
        if (JSON.parse(obj) !== null) {
        	tmpObj = JSON.parse(obj);
            tmpObj.type = "userExistenceResponse";
            tmpObj.exist = true;
		} else {
        	tmpObj = {
                type : "userExistenceResponse",
            	exist : false
			};
		}
        ws.send(JSON.stringify(tmpObj));
    });
}

UserLib.prototype.getUser = function (userid, ws)
{
    this.redisClient.hget('users', userid, function(err, obj) {
        ws.send(obj);
    });
}

UserLib.prototype.modifyUser = function( user, ws )
{
    delete user.action;
    this.redisClient.hset('users', user.id, JSON.stringify(user));

    ws.send(JSON.stringify(user));
}

module.exports = UserLib;