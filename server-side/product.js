function ProductLib( redisClient ) {
	this.redisClient = redisClient;
	this.productIdCounter = 0;
}

ProductLib.prototype.addProduct = function( object, productId ) 
{
	let that = this;

	object.productId = productId;
	let amazonTag = object.amazonKey;

	this.redisClient.hset('products', productId, JSON.stringify(object));

	this.redisClient.hget('amazonTags', amazonTag, function(err, obj) {
        if (obj !== null) {
        	let objTmp = JSON.parse(obj);
            objTmp.products.push(productId);
            that.redisClient.hset('amazonTags', amazonTag, JSON.stringify(objTmp));

            console.log(objTmp);
		} else {
        	let newObj = {
        		products : []
			};
        	newObj.products.push(productId);
            that.redisClient.hset('amazonTags', amazonTag, JSON.stringify(newObj));

            console.log(newObj);
		}
    });

	this.redisClient.hget('products', productId, function(err, obj) {
	    console.log(obj);
	});
}

ProductLib.prototype.getAllProductsRelatedWith = function( amazonTag, ws )
{
	let that = this;

	let objectToReturn = {
		type : 'product',
		action : 'setAllRelatedWith',
		products : []
	};

    this.redisClient.hget('amazonTags', amazonTag, function(err, obj) {

    	let objTmp = JSON.parse(obj);

    	let counter = 1;

    	if (objTmp !== null) {
			for (let i in objTmp.products) {
				that.redisClient.hget('products', objTmp.products[i], function(err, obj) {
					let productObjTmp = obj;
					that.redisClient.hget('stores', JSON.parse(obj).storeId, function(err, obj) {
						let productObj = JSON.parse(productObjTmp);
						productObj.storeInfo = JSON.parse(obj);
						objectToReturn.products.push(productObj);
						if (objTmp.products.length === counter) {
							ws.send(JSON.stringify(objectToReturn));
						}
						counter++;
					});
				});
			}

			if (objTmp.products.length === 0) {
                ws.send(JSON.stringify(objectToReturn));
			}
        } else {
    		ws.send(JSON.stringify(objectToReturn));
		}
    });
}

ProductLib.prototype.getAllProductsAdmin = function( storeId, ws )
{
    let that = this;

    let objectToReturn = {
        type : 'product',
        action : 'setAllProductsAdmin',
        products : []
    };

    this.redisClient.hget('stores', storeId, function(err, obj) {

        let storeTmp = JSON.parse(obj);

        let counter = 1;

        if (storeTmp !== null) {
            for (let i in storeTmp.products) {
                that.redisClient.hget('products', storeTmp.products[i], function(err, obj) {

					let productObj = JSON.parse(obj);
					objectToReturn.products.push(productObj);
					if (storeTmp.products.length === counter) {
						ws.send(JSON.stringify(objectToReturn));
					}
					counter++;

                });
            }

            if (storeTmp.products.length === 0) {
                ws.send(JSON.stringify(objectToReturn));
            }
        } else {
            ws.send(JSON.stringify(objectToReturn));
        }
    });
}

ProductLib.prototype.calculateProductId = function( object, callback )
{	
	var mainInstance = this;

	this.redisClient.incr('productIdCounter');

	this.redisClient.get('productIdCounter', function(err, value) {
		mainInstance.updateProductIdCounter(value);
	});

    callback(object, this.productIdCounter);

    return this.productIdCounter;
}

ProductLib.prototype.updateProductIdCounter = function(val)
{
	this.productIdCounter = val;
}

module.exports = ProductLib;